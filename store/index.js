import Vuex from 'vuex'
import wp from '~/lib/wp'

// Mutation Types
export const types = {
  SITE_DATA_UPDATE: 'SITE_DATA_UPDATE',
  POST_LIST_UPDATE: 'POST_LIST_UPDATE',
  AUTHORS_UPDATE: 'AUTHORS_UPDATE',
  CATEGORY_UPDATE: 'CATEGORY_UPDATE',
  COMMENTS_UPDATE: 'COMMENTS_UPDATE',
  CURRENT_POST_UPDATE: 'CURRENT_POST_UPDATE',
  CURRENT_PAGE_UPDATE: 'CURRENT_PAGE_UPDATE'
}

const createStore = () => {
  return new Vuex.Store({
    state: {
      site_data: {},
      post_list: [],
      page_list: [],
      authors: {},
      comments: [],
      category: {},
      current_post: {},
      current_page: {},
      imagePath: 'https://s3.amazonaws.com/soehlwebassets/img/'
    },
    mutations: {
      [types.SITE_DATA_UPDATE](state, payload) {
        state.site_data = { ...payload
        }
      },
      [types.POST_LIST_UPDATE](state, payload) {
        state.post_list = [...payload]
      },
      [types.AUTHORS_UPDATE](state, payload) {
        state.authors = { ...payload
        }
      },
      [types.CATEGORY_UPDATE](state, payload) {
        state.category = { ...payload
        }
      },
      [types.COMMENTS_UPDATE](state, id) {
        state.comments = id
      },
      [types.CURRENT_POST_UPDATE](state, payload) {
        state.current_post = { ...payload
        }
      },
      [types.CURRENT_PAGE_UPDATE](state, payload) {
        state.current_page = { ...payload
        }
      }
    },
    actions: {
      nuxtServerInit({
        commit
      }) {
        const getSiteData = wp.siteData()
          .then(res => {
            commit(types.SITE_DATA_UPDATE, res.site_data)
          })
        const getAuthors = wp.authors()
          .then(res => {
            const authors = res.users.reduce((out, val) => {
              return {
                ...out,
                [val.id]: val
              }
            }, {})
            commit(types.AUTHORS_UPDATE, authors)
          })
        const getComments = wp.comments()
          .then(res => {
            const comments = res.comments.reduce((out, val) => {
              return {
                ...out,
                [val.post]: val
              }
            }, {})
            commit(types.COMMENTS_UPDATE, comments)
          })
        const getCategory = wp.category()
          .then(res => {
            const category = res.categories.reduce((out, val) => {
              return {
                ...out,
                [val.id]: val
              }
            }, {})
            commit(types.CATEGORY_UPDATE, category)
          })

        return Promise.all([getSiteData, getAuthors, getCategory, getComments])
      }
    }
  })
}

export default createStore