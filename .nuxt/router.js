import Vue from 'vue'
import Router from 'vue-router'
import { interopDefault } from './utils'

const _b5f662e8 = () => interopDefault(import('..\\pages\\blog.vue' /* webpackChunkName: "pages_blog" */))
const _3f1b40c6 = () => interopDefault(import('..\\pages\\cryptocurrency\\index.vue' /* webpackChunkName: "pages_cryptocurrency_index" */))
const _73f17980 = () => interopDefault(import('..\\pages\\forex\\index.vue' /* webpackChunkName: "pages_forex_index" */))
const _5d81e26f = () => interopDefault(import('..\\pages\\login.vue' /* webpackChunkName: "pages_login" */))
const _7820f6a5 = () => interopDefault(import('..\\pages\\stock-market\\index.vue' /* webpackChunkName: "pages_stock-market_index" */))
const _56da0078 = () => interopDefault(import('..\\pages\\forex\\economic-calendar.vue' /* webpackChunkName: "pages_forex_economic-calendar" */))
const _01446b6d = () => interopDefault(import('..\\pages\\stock-market\\filings.vue' /* webpackChunkName: "pages_stock-market_filings" */))
const _9216ba1a = () => interopDefault(import('..\\pages\\a\\_slug.vue' /* webpackChunkName: "pages_a__slug" */))
const _3bd4ed5c = () => interopDefault(import('..\\pages\\category\\_slug.vue' /* webpackChunkName: "pages_category__slug" */))
const _49ed0064 = () => interopDefault(import('..\\pages\\cryptocurrency\\_coin.vue' /* webpackChunkName: "pages_cryptocurrency__coin" */))
const _3e58be32 = () => interopDefault(import('..\\pages\\forex\\_pair.vue' /* webpackChunkName: "pages_forex__pair" */))
const _c026e088 = () => interopDefault(import('..\\pages\\stock-market\\_ticker.vue' /* webpackChunkName: "pages_stock-market__ticker" */))
const _d84b8150 = () => interopDefault(import('..\\pages\\index.vue' /* webpackChunkName: "pages_index" */))

Vue.use(Router)

if (process.client) {
  window.history.scrollRestoration = 'manual'
}
const scrollBehavior = function (to, from, savedPosition) {
  // if the returned position is falsy or an empty object,
  // will retain current scroll position.
  let position = false

  // if no children detected and scrollToTop is not explicitly disabled
  if (
    to.matched.length < 2 &&
    to.matched.every(r => r.components.default.options.scrollToTop !== false)
  ) {
    // scroll to the top of the page
    position = { x: 0, y: 0 }
  } else if (to.matched.some(r => r.components.default.options.scrollToTop)) {
    // if one of the children has scrollToTop option set to true
    position = { x: 0, y: 0 }
  }

  // savedPosition is only available for popstate navigations (back button)
  if (savedPosition) {
    position = savedPosition
  }

  return new Promise((resolve) => {
    // wait for the out transition to complete (if necessary)
    window.$nuxt.$once('triggerScroll', () => {
      // coords will be used if no selector is provided,
      // or if the selector didn't match any element.
      if (to.hash) {
        let hash = to.hash
        // CSS.escape() is not supported with IE and Edge.
        if (typeof window.CSS !== 'undefined' && typeof window.CSS.escape !== 'undefined') {
          hash = '#' + window.CSS.escape(hash.substr(1))
        }
        try {
          if (document.querySelector(hash)) {
            // scroll to anchor by returning the selector
            position = { selector: hash }
          }
        } catch (e) {
          console.warn('Failed to save scroll position. Please add CSS.escape() polyfill (https://github.com/mathiasbynens/CSS.escape).')
        }
      }
      resolve(position)
    })
  })
}

export function createRouter() {
  return new Router({
    mode: 'history',
    base: '/',
    linkActiveClass: 'nuxt-link-active',
    linkExactActiveClass: 'nuxt-link-exact-active',
    scrollBehavior,

    routes: [{
      path: "/blog",
      component: _b5f662e8,
      name: "blog"
    }, {
      path: "/cryptocurrency",
      component: _3f1b40c6,
      name: "cryptocurrency"
    }, {
      path: "/forex",
      component: _73f17980,
      name: "forex"
    }, {
      path: "/login",
      component: _5d81e26f,
      name: "login"
    }, {
      path: "/stock-market",
      component: _7820f6a5,
      name: "stock-market"
    }, {
      path: "/forex/economic-calendar",
      component: _56da0078,
      name: "forex-economic-calendar"
    }, {
      path: "/stock-market/filings",
      component: _01446b6d,
      name: "stock-market-filings"
    }, {
      path: "/a/:slug?",
      component: _9216ba1a,
      name: "a-slug"
    }, {
      path: "/category/:slug?",
      component: _3bd4ed5c,
      name: "category-slug"
    }, {
      path: "/cryptocurrency/:coin",
      component: _49ed0064,
      name: "cryptocurrency-coin"
    }, {
      path: "/forex/:pair",
      component: _3e58be32,
      name: "forex-pair"
    }, {
      path: "/stock-market/:ticker?",
      component: _c026e088,
      name: "stock-market-ticker"
    }, {
      path: "/",
      component: _d84b8150,
      name: "index"
    }],

    fallback: false
  })
}
