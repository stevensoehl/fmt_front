import axios from 'axios'

class IEX {

  constructor(siteurl) {
    this.apiBase = `${siteurl}`
  }

  news(ticker, limit) {
    return axios.get(`${this.apiBase}/stock/${ticker}/news/last/${limit}`, {
        params: {
          //page: 1,
        }
      })
      .then(json => {
        return {
          news: json.data,
        }
        //this.configPagination(headers)
      })
      .catch(e => {
        return {
          error: e
        }
      })
  }
}

const iex = new IEX('https://api.iextrading.com/1.0')
export default iex