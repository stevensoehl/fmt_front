import axios from 'axios'
//var safeJsonStringify = require('safe-json-stringify');

class WpApi {
  constructor(siteurl) {
    this.apiBase = `${siteurl}/wp-json`
  }

  posts(options, type) {
    return axios.get(`${this.apiBase}/wp/v2/posts`, {
        params: {
          page: 1,
          //per_page: 5,
          ...options
        }
      })
      .then(json => {
        return {
          posts: json.data,
          headers: json.headers
        }
        //this.configPagination(headers)
      })
      .catch(e => {
        return {
          error: e
        }
      })
  }

  pages(options) {
    return axios.get(`${this.apiBase}/wp/v2/pages`, {
        params: {
          page: 1,
          ...options
        }
      })
      .then(json => {
        return {
          pages: json.data
        }
      })
      .catch(e => {
        return {
          error: e
        }
      })
  }

  category(options) {
    const params = {
      page: 1,
      per_page: 20,
      ...options
    }
    return axios.get(`${this.apiBase}/wp/v2/categories`, {
        params
      })
      .then(json => {
        return {
          categories: json.data
        }
      })
      .catch(e => {
        return {
          error: e
        }
      })
  }

  comments(options) {
    const params = {
      ...options
    }
    return axios.get(`${this.apiBase}/wp/v2/comments`, {
        params
      })
      .then(json => {
        return {
          comments: json.data,
          commentTotal: json.headers
        }
      })
      .catch(e => {
        return {
          error: e
        }
      })
  }

  authors(options) {
    const params = {
      page: 1,
      per_page: 20,
      ...options
    }
    return axios.get(`${this.apiBase}/wp/v2/users`, {
        params
      })
      .then(json => {
        return {
          users: json.data
        }
      })
      .catch(e => ({
        error: e
      }))
  }

  siteData() {
    return axios.get(this.apiBase)
      .then(json => {
        const {
          name,
          description,
          url,
          home,
          gmt_offset,
          timezone_string
        } = json.data
        return {
          site_data: {
            name,
            description,
            url,
            home,
            gmt_offset,
            timezone_string
          }
        }
      })
      .catch(e => ({
        error: e
      }))
  }
}

const wp = new WpApi('https://wpapi.finmarkettrader.com')

export default wp